import { useState } from "react";

const useModal = () => {
  const [isShowingCreate, setIsShowingCreate] = useState(false);
  const [isShowingEdit, setIsShowingEdit] = useState(false);

  function toggleCreate() {
    setIsShowingCreate(!isShowingCreate);
  }

  function toggleEdit() {
    setIsShowingEdit(!isShowingEdit);
  }

  return {
    isShowingCreate,
    toggleCreate,
    isShowingEdit,
    toggleEdit,
  };
};

export default useModal;
