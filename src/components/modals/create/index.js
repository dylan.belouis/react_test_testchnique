import React, { useState, useEffect } from "react";
import "./style.css";
import { databaseRef } from "../../../config/firebase";
import ReactDOM from "react-dom";

const AddItem = (props) => {
  const [userId, setUserId] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [isValid, setIsValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [comfirmMessage, setComfirmMessage] = useState("");
  const testRef = databaseRef.child(`users/`);

  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const modelData = {
    userId: userId,
    firstName: firstName,
    lastName: lastName,
    email: email,
  };

  const pushNewPerson = (e) => {
    setIsLoading(true);
    e.preventDefault();
    if (email && firstName && lastName && userId) {
      testRef
        .push(modelData)
        .then(() => {
          console.log("Success");
          setEmail("");
          setFirstName("");
          setLastName("");
          setUserId("");
          setIsLoading(false);
          setComfirmMessage("Création réussie");
        })
        .catch((error) => {
          console.log(error);
          setIsLoading(false);
          setComfirmMessage("Erreur lors de la création");
        });
    } else {
      setIsLoading(false);
      setComfirmMessage("Veuillez remplir tous les champs");
      setIsValid(false);
    }
  };

  return props.isShowingCreate
    ? ReactDOM.createPortal(
        <>
          <div className="AddItemEntrance">
            <div className="AddItem_paper_modal">
              <div className="AddItem_description">
                <h2>Enregistrez votre nouvel utilisateurs</h2>
                <p>
                  Entrez les informations suivantes dans le formulaire suivant.
                </p>
                <ul className="Main_displaying_ul">
                  <li className="Main_displaying_li">Pseudo</li>
                  <li className="Main_displaying_li">Prénom</li>
                  <li className="Main_displaying_li">Nom</li>
                  <li className="Main_displaying_li">Email</li>
                </ul>
                <div className="Main_close__modal_div">
                  <button
                    className="Main_close__modal_button"
                    onClick={props.hide}
                  >
                    Fermer
                  </button>
                </div>
              </div>
              <form className="AddItem_displaying_form">
                <span>Pseudo</span>
                <input
                  className="AddItem_input"
                  type="text"
                  placeholder="Pseudo"
                  value={userId}
                  onChange={(e) => setUserId(e.target.value)}
                />
                {isValid === false && userId === "" && (
                  <p className="AddItem_error_message">
                    Veuillez remplir ce champ
                  </p>
                )}
                <span>Prénom</span>
                <input
                  className="AddItem_input"
                  type="text"
                  placeholder="Prénom"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
                {isValid === false && firstName === "" && (
                  <p className="AddItem_error_message">
                    Veuillez remplir ce champ
                  </p>
                )}
                <span>Nom</span>
                <input
                  className="AddItem_input"
                  type="text"
                  placeholder="Nom"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
                {isValid === false && lastName === "" && (
                  <p className="AddItem_error_message">
                    Veuillez remplir ce champ
                  </p>
                )}
                <span>Email</span>
                <input
                  className="AddItem_input"
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {isValid === false && email === "" && (
                  <p className="AddItem_error_message">
                    Veuillez remplir ce champ
                  </p>
                )}
                <p>{comfirmMessage}</p>
                <button className="AddItem_form_button" onClick={pushNewPerson}>
                  {isLoading ? "Chargement..." : "Enregistrer"}
                </button>
              </form>
            </div>
          </div>
        </>,
        document.body
      )
    : null;
};

export default AddItem;
