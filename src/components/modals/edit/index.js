import React, { useState, useEffect } from "react";
import "./style.css";
import { databaseRef } from "../../../config/firebase";
import ReactDOM from "react-dom";

const EditItem = (props) => {
  const data = props.profil;
  const [useUserId, setUserId] = useState(data.userId);
  const [useFirstName, setFirstName] = useState(data.firstName);
  const [useLastName, setLastName] = useState(data.lastName);
  const [useEmail, setEmail] = useState(data.email);
  const [isValid, setIsValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [comfirmMessage, setComfirmMessage] = useState("");

  useEffect(() => {
    setUserId(data.userId);
    setFirstName(data.firstName);
    setLastName(data.lastName);
    setEmail(data.email);
    console.log("Edit refresh");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  const modelData = {
    userId: useUserId,
    firstName: useFirstName,
    lastName: useLastName,
    email: useEmail,
  };

  const sendEdit = (e) => {
    setIsLoading(true);
    e.preventDefault();
    if (useEmail && useFirstName && useLastName && useUserId) {
      const reftest = databaseRef.child(`users/${data.id}`);
      reftest
        .update(modelData)
        .then(() => {
          setEmail("");
          setFirstName("");
          setLastName("");
          setUserId("");
          setIsLoading(false);
          setComfirmMessage("Modification réussie");
        })
        .catch((error) => {
          console.log("Update failed: " + error.message);
          setIsLoading(false);
          setComfirmMessage("Erreur lors de la modification");
        });
    } else {
      setIsLoading(false);
      setIsValid(false);
      setComfirmMessage("Veuillez remplir tous les champs");
    }
  };

  return props.isShowingEdit
    ? ReactDOM.createPortal(
        <>
          <div className="EditItemEntrance">
            <div className="EditItem_paper_modal">
              <div className="EditItem_description">
                <h2>Modifier un utilisateur</h2>
                <p>
                  Entrez les informations suivantes dans le formulaire suivant.
                </p>
                <ul>
                  <li>Pseudo</li>
                  <li>Prénom</li>
                  <li>Nom</li>
                  <li>Email</li>
                </ul>
                <div className="Main_close__modal_div">
                  <button
                    className="Main_close__modal_button"
                    onClick={props.hide}
                  >
                    Fermer
                  </button>
                </div>
              </div>
              <form className="EditItem_displaying_form">
                <span>Pseudo</span>
                <input
                  className="EditItem_input"
                  type="text"
                  placeholder="Pseudo"
                  value={useUserId}
                  onChange={(e) => setUserId(e.target.value)}
                />
                {isValid === false && useUserId === "" && (
                  <p className="EditItem_error">Veuillez remplir ce champ</p>
                )}
                <span>Prénom</span>
                <input
                  className="EditItem_input"
                  type="text"
                  placeholder="Prénom"
                  value={useFirstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
                {isValid === false && useFirstName === "" && (
                  <p className="EditItem_error">Veuillez remplir ce champ</p>
                )}
                <span>Nom</span>
                <input
                  className="EditItem_input"
                  type="text"
                  placeholder="Nom"
                  value={useLastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
                {isValid === false && useLastName === "" && (
                  <p className="EditItem_error">Veuillez remplir ce champ</p>
                )}
                <span>Email</span>
                <input
                  className="EditItem_input"
                  type="email"
                  placeholder="Email"
                  value={useEmail}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {isValid === false && useEmail === "" && (
                  <p className="EditItem_error">Veuillez remplir ce champ</p>
                )}
                <p>{comfirmMessage}</p>
                <button className="EditItem_form_button" onClick={sendEdit}>
                  {isLoading ? "Chargement..." : "Modifier"}
                </button>
              </form>
            </div>
          </div>
        </>,
        document.body
      )
    : null;
};

export default EditItem;
