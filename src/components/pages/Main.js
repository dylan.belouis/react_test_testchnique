import React, { useState, useEffect } from "react";
import { getDatabase, ref, onValue } from "firebase/database";
import ListItem from "../list-item";
import useModal from "../modals/useModal";
import AddItem from "../modals/create";
import "./style.css";

const Main = () => {
  const [profils, setProfils] = useState([]);
  const [itemKey, setItemKey] = useState("");
  const db = getDatabase();
  const usersRef = ref(db, "users/");
  const { isShowingCreate, toggleCreate } = useModal();

  useEffect(() => {
    onValue(usersRef, (snapshot) => {
      const data = snapshot.val();
      data ? setProfils(Object.entries(data)) : setProfils([]);
    });
    console.log("ListItem refresh");
    // eslint-disable-next-line
  }, []);

  return (
    <div className="Main">
      <div className="Main_lastname_list">
        <button className="Main_create_person_button" onClick={toggleCreate}>
          Create
        </button>
        <AddItem
          isShowingCreate={isShowingCreate}
          hide={toggleCreate}
          toggleIsOpen={toggleCreate}
        />
        {profils &&
          profils.map((profil, id) => (
            <div
              onClick={() => setItemKey(profil[0])}
              className="Main_list_item_div"
            >
              <p className="Main_list_item_p">{profil[1].lastName}</p>
              {/* <div className="Main_seeMore_button_div">
                <button
                  onClick={() => setItemKey(profil[0])}
                  className="Main_list_item_button"
                >
                  Voir plus
                </button>
              </div> */}
            </div>
          ))}
      </div>
      <div className="Main_detail_list">
        <ListItem databaseRef={itemKey} />
      </div>
    </div>
  );
};

export default Main;
