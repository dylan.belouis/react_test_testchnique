import { databaseRef } from "../../config/firebase";
import { getDatabase, ref, onValue } from "firebase/database";

export const deletePerson = (id) => {
  const reftest = databaseRef.child(`users/${id}`);
  reftest
    .remove()
    .then(() => {
      console.log("Remove succeeded.");
    })
    .catch((error) => {
      console.log("Remove failed: " + error.message);
    });
};

export const getData = () => {
  const db = getDatabase();
  let data;
  const usersRef = ref(db, "users/");
  onValue(usersRef, (snapshot) => {
    data = snapshot.val();
  });
  return Object.entries(data) ? data : [];
};
