import React, { useState, useEffect } from "react";
import EditItem from "../modals/edit";
import { getDatabase, ref, onValue } from "firebase/database";
import "./style.css";
import { deletePerson } from "../function/firebaseFunction";
import useModal from "../modals/useModal";

const ListItem = (props) => {
  const [profils, setProfils] = useState([]);
  const [selectedProfil, setSelectedProfil] = useState({});
  let validityData = props.databaseRef;
  const db = getDatabase();
  const usersRef = ref(db, `users/${props.databaseRef}`);
  const { isShowingEdit, toggleEdit } = useModal();

  useEffect(() => {
    onValue(usersRef, (snapshot) => {
      const data = snapshot.val();
      data ? setProfils(data) : setProfils([]);
    });
    console.log(profils);
    // eslint-disable-next-line
  }, [props]);

  const editPerson = (profil, id, e) => {
    e.preventDefault();
    console.log(profil);
    setSelectedProfil({
      userId: profil.userId,
      id: id,
      firstName: profil.firstName,
      lastName: profil.lastName,
      email: profil.email,
    });
    toggleEdit();
  };

  return (
    <section>
      {validityData ? (
        <div className="list_person_displaying_div">
          <EditItem
            isShowingEdit={isShowingEdit}
            hide={toggleEdit}
            profil={selectedProfil}
          />
          <div className="list_person_item_div" key={props.databaseRef}>
            <p className="list_person_item_p">{props.databaseRef}</p>
            <p className="list_person_item_p">{profils.userId}</p>
            <p className="list_person_item_p">{profils.firstName}</p>
            <p className="list_person_item_p">{profils.lastName}</p>
            <p className="list_person_item_p">{profils.email}</p>
            <div className="list_displaying_button">
              <div className="list_box_button_div">
                <button
                  onClick={(e) => editPerson(profils, props.databaseRef, e)}
                  className="list_person_item_button list_person_item_button_edit"
                >
                  Edit
                </button>
              </div>
              <div className="list_box_button_div">
                <button
                  onClick={() => deletePerson(props.databaseRef)}
                  className="list_person_item_button list_person_item_button_delete"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div>
          <p>Cliquez sur un item pour voir plus ou les modifier</p>
        </div>
      )}
    </section>
  );
};

export default ListItem;
