import firebase from "firebase/compat/app";
import "firebase/compat/database";
import config from "./config";

const Firebase = firebase.initializeApp(config.firebase);

// export const todosRef = () => {
//   databaseRef.child("event");
// };

export const databaseRef = firebase.database().ref();

export default Firebase;
